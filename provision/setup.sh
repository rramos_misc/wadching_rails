#!/usr/bin/env bash
echo 'Actualizando repositorio del sistema operativo'
sudo apt-get update

echo 'Instalando librerias necesarias'
sudo apt-get install libpng12-dev libglib2.0-dev zlib1g-dev libbz2-dev libtiff4-dev libjpeg8-dev

echo 'Instalando ImageMagick'
sudo apt-get install imagemagick
