class CreateHeaders < ActiveRecord::Migration
  def change
    create_table :headers do |t|
      t.string :url
      t.string :css_identifier
      t.string :css_class
      t.string :title

      t.timestamps null: false
    end
  end
end
