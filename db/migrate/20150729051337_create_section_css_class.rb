class CreateSectionCssClass < ActiveRecord::Migration
  def change
    create_table :section_css_classes do |t|
      t.belongs_to :section, index: true
      t.belongs_to :css_class, index: true
    end
  end
end
