class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :title
      t.text :content
      t.integer :position

      t.timestamps null: false
    end
  end
end
