class CreateCssClasses < ActiveRecord::Migration
  def change
    create_table :css_classes do |t|
      t.string :name
      t.string :type_of

      t.timestamps null: false
    end
  end
end
