# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
@videos = []
@headers = []
@sections = []

def create_video url, title, description, image_preview = 'no_video.png'
  video = Video.new
  video.url = url
  video.title = title
  video.description = description
  video.image_preview = File.new("#{Rails.root}/app/assets/images/#{image_preview}")
  video.save
  @videos.push video
end

def create_header url, title, css_identfier = nil, css_class = nil
  header = Header.new
  header.url = url
  header.title = title
  header.css_identifier = css_identfier
  header.css_class = css_class
  header.save
  @headers.push header
end

def create_section title, content, position = 1
  section = Section.new
  section.title = title
  section.content = content
  section.position = position
  section.save
  @sections.push section
end

################ VIDEOS ################
video_description = 'Colonel Sanders is back, America! He’s back to make sure his Kentucky Fried Chicken® is still as delicious as it ever was. And he made this commercial about it. Because, boy howdy, have things changed since the Colonel’s been gone! Nowadays, you’ve got your self-parking electronic automobiles. Your casual Fridays. Your computer phones and invisible cosmetic braces for adults. Your yoga pants, nanny cams, books on tape, mail-order remote-control submarines, unscented roll-on antiperspirants, leather ottomans with hideaway storage, texting, foam rollers, adjustable waistbands, night-vision goggles, scratch-resistant countertops, action-packed thrillers about the deadly nature of virtual reality, travel-size toothpastes and all kinds of lumbar support. But the one thing that hasn’t changed is the Colonel’s Kentucky Fried Chicken®. It’s still finger lickin’ good!™'
create_video 'https://www.youtube.com/watch?v=Dh2rHsxYh6U', 'The State of Kentucky Fried Chicken Address', video_description, 'kfc_image_preview.jpg'

video_description = "It IS super fun to blow up something that provides us with no strategic advantage. Come with a plan or leave in defeat, download Boom Beach for free http://supr.cl/BoomBeach Watch the new Boom Beach TV Commercial and find out how much fun it is to blow something up that provides no strategic advantage - poor Sniper Tower never stood a chance against that Gunboat! Play on iOS, Android! Download for free on your iPhone, iPad or Android device! Advertisement was featured on TV in the NHL Playoffs, Champions League and more. Welcome to Boom Beach: come with a plan or leave in defeat! Fight the evil Blackguard with brains and brawn in this epic combat strategy game. Attack enemy bases to free enslaved islanders and unlock the secrets of this tropical paradise. Create a Task Force with players around the world to take on the enemy together. Scout, plan, then BOOM THE BEACH! PLEASE NOTE! Boom Beach is free to download and play. However, some game items can also be purchased for real money. If you do not want to use this feature, please set up password protection for purchases in your settings. FEATURES - Play with millions of other players, raid hundreds of enemy bases for loot - Battle for control of precious resources to upgrade your base against enemy attacks - Explore a huge tropical archipelago and discover the mysterious power of the Life Crystals - Face fearsome Blackguard Bosses and uncover their evil plans - Join other players to form an unstoppable Task Force to take on co-op missions Under our Terms of Services and Privacy Policy, Boom Beach is allowed for download and play only for persons 13 years or over of age without parental consent. Note: A network connection is required to play Parent's guide: http://www.supercell.net/parents Looks like that gunboat is aiming right at us. Don't be ridiculous. Destroying this sniper tower would provide no strategic advantage. I stand correct Johnson. It IS super fun to blow up something that provides us with no strategic advantage. Alright, let's head home boys!"
create_video 'https://www.youtube.com/watch?v=F0WaSy5jNLg', 'Boom Beach: Sniper Tower', video_description, 'boom_beach_image_preview.jpg'

video_description = "Somebody finally said Hog Rider? Really?! Attack. Defend. Strategize. Download for free for mobile devices. http://supr.cl/ThisArmy Watch the latest Clash of Clans TV Commercial featuring the Hog Rider and his friends! Wondered how mass Hog Rider attacks work? Now you know. The Hog Rider calls his fellow hogs into battle against Archers, Barbarians, Giants, Wizards and more. When Hog Riders aren't busy in the bath tub, working on their Tai Chi, arm wrestling barbarians, or playing cards with their hogs, they're ready to ride into battle in your favorite mobile game! Look for our ads during Champions League, NBA and NFL Playoffs! From rage-­filled Barbarians with glorious mustaches to pyromaniac wizards, raise your own army and lead your clan to victory! Build your village to fend off raiders, battle against millions of players worldwide, and forge a powerful clan with others to destroy enemy clans. PLEASE NOTE! Clash of Clans is free to download and play, however some game items can also be purchased for real money Also, under our Terms of Service and Privacy Policy, you must be at least 13 years of age to play or download Clash of Clans. A network connection is also required. FEATURES - Build your village into an unbeatable fortress - Raise your own army of Barbarians, Archers, Hog Riders, Wizards, Dragons and other mighty fighters - Battle with players worldwide and take their Trophies - Join together with other players to form the ultimate Clan - Fight against rival Clans in epic Clan Wars - Build 18 unique units with multiple levels of upgrades - Discover your favorite attacking army from countless combinations of troops, spells, Heroes and Clan reinforcements - Defend your village with a multitude of Cannons, Towers, Mortars, Bombs, Traps and Walls - Fight against the Goblin King in a campaign through the realm Chief, are you having problems? Visit http://supercell.helpshift.com/a/clas... Privacy Policy: http://www.supercell.net/privacy-policy/ Terms of Service: http://www.supercell.net/terms-of-ser... Parent’s Guide: http://www.supercell.net/parents"
create_video 'https://www.youtube.com/watch?v=Dh2rHsxYh6U', 'Clash of Clans: Ride of the Hog Riders', video_description, 'clash_of_clans.jpg'
################ VIDEOS ################
################ MENU #################
create_header nil, 'MY CONTRIBUTION'
create_header nil, 'HOW IT WORKS'
create_header nil, 'OUR ACHIEVMENTS'
create_header nil, 'CONTACT US'
################ MENU #################
################ SECTION #################
create_section 'MY CONTRIBUTION', 'Just by watching this Ad our sponsors will be donating 7 cents to Nepal earthquake victims. The people of Nepal and wADching thank you for your help. You can help even more by sharing this website on facebook or by subscribing to our weekly email to remind you to do this again next week. We seek your continuing support to continue helping those in need!'
create_section 'HOW IT WORKS', 'Anyone who wants to help can come to our website to watch an ad from our sponsors and donate to Nepal earthquake victims. We get paid for these advertisements, and then give all the profits to charities that support Vietnam earthquake victims which we support. No matter what our costs are, we will give 90% of the revenue to these charities. We use the rest of the revenue for two purposes: (1) to cover costs, which include hosting/server costs; and (2) to reinvest in getting more people to the website, so that we can give more to charity in the long term. We give most of our funds to Help Nepal Network. They are an amazing organization that has raised over $528,000 for the Nepal earthquake victims. With these finds they have mobilized dozens of groups of volunteers, sending tents, food and medical supplies to affected areas. More details can be found on www.HelpNepal.net We seek your continuing support to continue helping those in need!'
create_section 'OUR ACHIEVEMENTS', nil
create_section 'CONTACT US', nil
################ SECTION #################

AdminUser.create email: 'drodan@gmail.com', password:'Wadching+'