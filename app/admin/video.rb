ActiveAdmin.register Video do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  permit_params :url, :description, :title, :image_preview

  index do
    id_column
    column :title
    column :image do |video|
      image_tag(video.image_preview.url(:thumb))
    end
    actions
  end

  form do |f|
    f.inputs "Video details" do
      f.input :url
      f.input :title
      f.input :description, as: 'text'
      f.input :image_preview, :required => false, :as => :file
      # Will preview the image when the object is edited
    end
    f.actions
  end

  show do |video|
    attributes_table do
      row :title
      row :description
      row :image do
        image_tag(video.image_preview.url(:thumb))
      end
      # Will display the image on show object page
    end
  end

end
