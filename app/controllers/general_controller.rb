class GeneralController < ApplicationController
  before_action :get_menu

  def get_menu
    @headers = Header.all
  end

  def index
    @videos = Video.all
    @sections = Section.order :position
    if params[:id]
      @current_video = Video.find params[:id]
    else
      @current_video = Video.first
    end

  end
end
