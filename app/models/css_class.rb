# == Schema Information
#
# Table name: css_classes
#
#  id         :integer          not null, primary key
#  name       :string
#  type_of    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CssClass < ActiveRecord::Base
  has_and_belongs_to_many :sections
end
