# == Schema Information
#
# Table name: videos
#
#  id                         :integer          not null, primary key
#  url                        :string
#  title                      :string
#  description                :text
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  image_preview_file_name    :string
#  image_preview_content_type :string
#  image_preview_file_size    :integer
#  image_preview_updated_at   :datetime
#

class Video < ActiveRecord::Base
  has_attached_file :image_preview, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :image_preview, :content_type => /\Aimage\/.*\Z/
end
