# == Schema Information
#
# Table name: headers
#
#  id             :integer          not null, primary key
#  url            :string
#  css_identifier :string
#  css_class      :string
#  title          :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Header < ActiveRecord::Base
end
